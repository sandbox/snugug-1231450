jQuery(function() {
  $ = jQuery;
  
  // Initialize regions and path to the remove image
  var initObject = '';
  var pathToClose = '';
  $.post('blockdrop/init', function(initResponse) {
    var temp = new Array();
    temp = initResponse.split('&');
    initObject = $.parseJSON(temp[0]);
    pathToClose = decodeURIComponent(temp[1]);
    
    $.each(initObject, function(key, value) {
      // console.info(key);
      key = key.replace('_', '-');
       // Adds the remove image to each block
        $('div[class*=region-'+key+'] .block').append('<img src="'+pathToClose+'" class="blockdrop-remove" />');
        
        // Creates Link to Add Blocks
        if ( $('#region-'+key+' .blockdrop-add').length == 0 ) {
         $('#region-'+key+'').append('<div class="blockdrop-add">Add Blocks +</div>'); 
        }

        // Hides and shows the remove image on hover
        $('div[class*=region-'+key+'] .block').hover(function() {
          $(this).find('.blockdrop-remove').css('display', 'block');
        }, function() {
          $(this).find('.blockdrop-remove').css('display', 'none');
        });

        // Allows for block sorting and posts for a DB save
        $('div[class*=region-'+key+']').sortable({ opacity: 0.6, cursor: 'move', update: function() {
          var order = $(this).sortable('serialize');
          var region = $(this).attr('class');
          $(this).disableSelection();
          $.post('blockdrop/sort', {'order': order, 'region': region}, function(sortResponse) {
            $(this).enableSelection();
          });
        } });
    });
    
   
  });
  
  
  
  // Allows for blocks to be listed and adds the list popp
  $('.blockdrop-add').live('click', function() {
    var region = $(this).parent().attr('id');
    // console.info(region);
    $.post('blockdrop/list', {'region': region}, function(listResponse) {
      $('.blockdrop-add').remove();
      $('body').append(listResponse);
      blockdrop_vertical_align($('#blockdrop-popup'));
    });
  });
  
  // Allows for checkboxes in the add box to be checked by pressing the name
  $('#blockdrop-list tr').live('click', function() {
    var $checkbox = $(this).find(':checkbox');
    $checkbox.attr('checked', !$checkbox.attr('checked'));
  });
  
  // Changes the region when the region select box is changed
  $('.blockdrop-regions select').live('change', function() {
    // console.info($(this).attr('value'));
    var popupRegion = 'Add Blocks to Region ';
    popupRegion += $(this).attr('value');
    $('#blockdrop-popup-heading').text(popupRegion);
  });
  
  // Closes and removes Block Popup when Cancel is pressed
  $('#blockdrop-popup input[name="btn_cancel"]').live('click', function() {
    $('#blockdrop-popup').remove();
    $.each(initObject, function(key, value) {
      key = key.replace('_', '-');
      $('#region-'+key+'').append('<div class="blockdrop-add">Add Blocks +</div>');
    });
  });
  
  // Allows for items to be added, calls the DB to save, reloads page
  $('#blockdrop-popup input[name="btn_add"]').live('click', function() {
    var region = $('#blockdrop-popup-heading').text();
    var i = 0;
    var postArray = '';
    $('#blockdrop-popup input[type="checkbox"]:checked').each(function() {
      if (i != 0) {
        postArray += '&';
      }
      postArray += $(this).attr('value');
      i++;
    });
    $.post('blockdrop/add', {'blocks': postArray, 'region': region}, function() {
      location.reload();
      $('#blockdrop-popup').remove();
      $.each(initObject, function(key, value) {
        key = key.replace('_', '-');
        $('#region-'+key+'').append('<div class="blockdrop-add">Add Blocks +</div>');
      });
    });
  });
  
  // Allows for removing of blocks, calls DB, hides block
  $('.blockdrop-remove').live('click', function() {
    var parent_id = $(this).parent().attr('id');
    var region = $(this).parent().parent().attr('class');
    $.post('blockdrop/remove', {'block': parent_id, 'region': region}, function(removeResponse) {
      $('#'+removeResponse).css('display', 'none');
    });
  });
  
  
});

// Function for vertical aligning
function blockdrop_vertical_align(element) {
  // var marginTop = jQuery(window).scrollTop() + ((jQuery(window).height() / 2) - (element.height() / 2));
  var marginTop = jQuery(document).scrollTop() + (jQuery(window).height() - element.outerHeight())/2;
  var marginLeft = (jQuery(window).width() - element.outerWidth())/2;
  element.css('left', marginLeft);
  element.css('top', marginTop);
  // console.info(element);
}